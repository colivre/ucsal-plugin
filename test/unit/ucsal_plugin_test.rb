#!/bin/env ruby
# encoding: utf-8

require File.dirname(__FILE__) + '/../../../../test/test_helper'

class UcsalPluginTest < ActiveSupport::TestCase
  def setup
    @name = "Joao Paulo Alves Silva"
    @splited = @name.split(" ")
    @plugin = UcsalPlugin.new

    @students = Community.create!(:name => "Estudantes")
    @professors = Community.create!(:name => "Professores")
    @employees = Community.create!(:name => "Funcionarios")
  end

  should "apply first rule" do
    assert_equal "joao-silva", @plugin.first_rule(@splited)
  end

  should "apply first rule even if there is only one name" do
    assert_equal "joao", @plugin.first_rule("Joao".split)
  end

  should "apply second rule" do
    assert_equal "joao-p-silva", @plugin.second_rule(@splited)
  end

  should "apply fourth rule if it's impossible to apply the second rule" do
    assert_equal "joao-silva1", @plugin.second_rule("Joao Silva".split(" "))
  end

  should "apply third rule" do
    assert_equal "joao-p-a-silva", @plugin.third_rule(@splited)
  end

  should "apply fourth rule if it's impossible to apply the third rule" do
    assert_equal "joao-silva1", @plugin.third_rule("Joao Paulo Silva".split(" "))
  end

  should "apply fourth rule" do
    assert_equal "joao-silva1", @plugin.fourth_rule(@splited)
  end

  should "properly create the identifier" do
    assert_equal "joao", @plugin.create_identifier("João")
    assert_equal "joao-silva", @plugin.create_identifier("João Silva")
    assert_equal "joao-silva", @plugin.create_identifier("João Paulo Silva")
    assert_equal "joao-silva", @plugin.create_identifier("João Paulo Alves Silva")

    fast_create(Person, {:identifier => "joao-silva"})
    fast_create(Person, {:identifier => "joao"})

    assert_equal "joao-p-silva", @plugin.create_identifier("João Paulo Silva")
    assert_equal "joao3", @plugin.create_identifier("João")

    fast_create(Person, {:identifier => "joao-p-silva"})
    assert_equal "joao-p-a-silva", @plugin.create_identifier(@name)

    fast_create(Person, {:identifier => "joao-p-a-silva"})
    fast_create(Person, {:identifier => "joao-silva-pereira"})
    fast_create(Person, {:identifier => "joao-silva-souza"})
    assert_equal "joao-silva4", @plugin.create_identifier(@name)
  end

  should "create identifier with dot to not students" do
    assert_equal "joao.silva", @plugin.create_identifier("João Silva", false)

    fast_create(Person, {:identifier => "joao-silva"})
    assert_equal "joao.p.silva", @plugin.create_identifier("João Paulo Silva", false)

    fast_create(Person, {:identifier => "joao-p-silva"})
    assert_equal "joao.p.a.silva", @plugin.create_identifier(@name, false)
  end

  should "return profile_data hash for users whose login is number with dash on identifiers" do
    assert_equal 'joao-silva', @plugin.ldap_plugin_set_profile_data({:fullname => @name}, {:user => {:login => "133249919-10"}, :profile_data => {}}).last[:profile_data][:identifier]

    assert_equal 'joao-silva', @plugin.ldap_plugin_set_profile_data({:fullname => @name}, {:user => {:login => "133249919-10"}}).last[:profile_data][:identifier]
  end

  should "return profile_data hash for users whose login is not number with dot on identifiers" do
    assert_equal 'joao.silva', @plugin.ldap_plugin_set_profile_data({:fullname => @name}, {:user => {:login => "JoaoS"}, :profile_data => {}}).last[:profile_data][:identifier]

    assert_equal 'joao.silva', @plugin.ldap_plugin_set_profile_data({:fullname => @name}, {:user => {:login => "JoaoS"}}).last[:profile_data][:identifier]
  end

  should "set the profile ucsal template" do
    person = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person

    @plugin.set_user_template(person, ["CN=Biblioteca_FD G,OU=Grupos", "CN=Pedagogia G,OU=Grupos"])

    person.reload

    assert_equal "profile-ucsal", person.theme
  end

  should "apply the correct template to user based on its profile" do
    person = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person
    funcionario = User.create!(:login => "modelo-funcionario", :email => "modelo-funcionario@example.com", :password => "test", :password_confirmation => "test", :person_data => { :is_template => true }).person
    professor = User.create!(:login => "modelo-professor", :email => "modelo-professor@example.com", :password => "test", :password_confirmation => "test", :person_data => { :is_template => true }).person
    estudante = User.create!(:login => "modelo-estudante", :email => "modelo-estudante@example.com", :password => "test", :password_confirmation => "test", :person_data => { :is_template => true }).person

    @plugin.set_user_template(person, ["CN=Biblioteca_FD G,OU=Grupos", "CN=Pedagogia G,OU=Grupos"])
    assert_equal funcionario, person.template

    @plugin.set_user_template(person, ["CN=Prf COPPEL G,OU=Grupos,OU=Professores", "CN=Pedagogia G,OU=Grupos", "CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica"])
    assert_equal professor, person.template

    @plugin.set_user_template(person, ["CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica", "CN=Prf COPPEL G,OU=Grupos,OU=Professores"])
    assert_equal professor, person.template

    @plugin.set_user_template(person, ["CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica", "CN=Biblioteca_FD G,OU=Grupos"])
    assert_equal estudante, person.template
  end

  should "include user with no description in Funcionarios community" do
    person = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person

    @plugin.join_profile_community(person, [])

    person.reload

    assert person.is_member_of? @employees
  end

  should "include a not Professor or Student user in Funcionarios community" do
    person = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person

    @plugin.join_profile_community(person, ["CN=Biblioteca_FD G,OU=Grupos", "CN=Pedagogia G,OU=Grupos"])

    person.reload

    assert person.is_member_of? @employees
  end

  should "include a students in Alunos community" do
    person1 = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person
    person2 = User.create!(:login => "maria-silva", :email => "maria-silva@example.com", :password => "test", :password_confirmation => "test").person

    @plugin.join_profile_community(person1, ["CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica", "CN=Biblioteca_FD G,OU=Grupos"])
    @plugin.join_profile_community(person2, ["CN=Biblioteca_FD G,OU=Grupos", "CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica"])

    person1.reload
    person2.reload

    assert person1.is_member_of? @students
    assert person2.is_member_of? @students
  end

  should "include a professors in Professores community" do
    person1 = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person
    person2 = User.create!(:login => "maria-silva", :email => "maria-silva@example.com", :password => "test", :password_confirmation => "test").person

    @plugin.join_profile_community(person1, ["CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica", "CN=Prf COPPEL G,OU=Grupos,OU=Professores"])
    @plugin.join_profile_community(person2, ["CN=Prf COPPEL G,OU=Grupos,OU=Professores", "CN=Alunos Graduacao Tecnologica L,OU=Grupos,OU=Graduacao Tecnologica"])

    person1.reload
    person2.reload

    assert person1.is_member_of? @professors
    assert person2.is_member_of? @professors
  end

  should "include ldap user in additional communities" do
    person1 = User.create!(:login => "jose-silva", :email => "jose-silva@example.com", :password => "test", :password_confirmation => "test").person
    person2 = User.create!(:login => "maria-silva", :email => "maria-silva@example.com", :password => "test", :password_confirmation => "test").person

    assert_difference 'Community.count', 1 do
      @plugin.join_description_communities(person1, ["COPPEL"])
    end

    assert_difference 'Community.count', 2 do
      @plugin.join_description_communities(person2, ["COPPEL - Planejamento de Cidades - Comunicacao Social"])
    end

    assert_no_difference 'Community.count' do
      @plugin.join_description_communities(person1, ["COPPEL - Planejamento de Cidades - Comunicacao Social"])
    end

    assert_equal 3, person1.communities.count
    assert_equal 3, person2.communities.count
  end

  should "return human names for communities name" do
    assert_equal ["First Community", "Morning/Night", "Medicine"], @plugin.get_description_names("FIRST COMMUNITY - MORNING/NIGHT - MEDICINE")

    assert_equal ["First Community", "Morning/Night", "Medicine"], @plugin.get_description_names(["FIRST COMMUNITY - MORNING/NIGHT - MEDICINE"])
  end

  should 'include users in equivalent communities' do
    person = fast_create(Person)
    @plugin.join_description_communities(person, ['MATUTINO/NOTURNO'])

    assert_equivalent ['matutino', 'noturno'], person.communities.map(&:identifier)
  end
end
