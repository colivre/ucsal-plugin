require File.dirname(__FILE__) + '/../../../../test/test_helper'
require 'account_controller'

# Re-raise errors  caught by the controller.
class AccountController; def rescue_action(e) raise e end; end

class AccountControllerTest < ActionController::TestCase
  # Be sure to include AuthenticatedTestHelper in test/test_helper.rb instead
  # Then, you can remove it from this and the units test.
  include AuthenticatedTestHelper
  all_fixtures

  def setup
    @controller = AccountController.new
    @request = ActionController::TestRequest.new
    @response = ActionController::TestResponse.new

    @controller = AccountController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
    @person = create_user('person').person

    @environment = Environment.default
    @environment.enable_plugin('UcsalPlugin')
    @environment.save!

    login_as(@person.user.login)
  end

  should 'display ucsal change_password screen when the user forgot password' do
    get :change_password
    assert_response :success
    assert_template 'change_password_ucsal'
    assert_tag :tag => 'input', :attributes => { :name => 'user[matricula]' }
    assert_tag :tag => 'input', :attributes => { :name => 'user[dt_nascimento]' }
    assert_tag :tag => 'input', :attributes => { :name => 'user[cpf]' }
  end

  should 'display change_password screen even if a logged user wants to change his password' do
    login_as 'johndoe'
    get :change_password
    assert_response :success
    assert_template 'change_password_ucsal'
    assert_tag :tag => 'input', :attributes => { :name => 'user[matricula]' }
    assert_tag :tag => 'input', :attributes => { :name => 'user[dt_nascimento]' }
    assert_tag :tag => 'input', :attributes => { :name => 'user[cpf]' }
  end
end
