class UcsalPlugin < Noosfero::Plugin
  def self.plugin_name
    'Ucsal'
  end

  def self.plugin_description
    _('Add Ucsal features.')
  end

  def ldap_plugin_set_profile_data(attrs, params)
    params = {} unless params
    params[:profile_data] = {} unless params[:profile_data]

    if attrs[:fullname]
      params[:profile_data][:identifier] = create_identifier(attrs[:fullname], student?(params[:user][:login]))
    end

    [attrs, params]
  end

  def ldap_plugin_update_user(user, attrs)
    return if user.nil? || attrs.nil?

    add_user_communities(user.person, attrs)
    set_user_template(user.person, attrs[:memberof])
  end

  def set_user_template(person, groups)
    template = get_template(groups)

    if template
      if template.theme != "profile-ucsal"
        template.theme = "profile-ucsal"
        template.save
      end

      person.apply_template(template)
    else
      person.theme = "profile-ucsal"
    end

    person.save
  end

  def add_user_communities(person, attrs)
    join_profile_community(person, attrs[:memberof])
    join_description_communities(person, attrs[:description])
  end

  def join_profile_community(person, groups)
    return unless groups

    profile = get_profile_type(groups)

    community = Community[profile.to_slug]
    community.affiliate(person, Profile::Roles.member(community.environment.id)) if community
  end

  def get_profile_type(groups)
    return unless groups

    profile = "Funcionarios"
    groups.each do |group|
      if group.include?("Professores")
        profile = "Professores"
        break
      elsif group.include?("Alunos")
        profile = "Estudantes"
      end
    end

    profile
  end

  def join_description_communities(person, description)
    return unless description

    names = get_description_names(description)

    names.each do |name|
      identifiers = get_equivalent_communities(name.to_slug)
      identifiers.each do |identifier|
        community = Community.where(:identifier => identifier).first_or_create do |c|
          c.name = name
        end
        community.affiliate(person, Profile::Roles.member(community.environment.id))
      end
    end
  end

  def get_description_names(description)
    if description.kind_of? Array
      names = description.first.split(" - ")
    else
      names = description.split(" - ")
    end

    names.map {|name| name.downcase.titleize }
  end

  def get_equivalent_communities(identifier)
    mapping = YAML.load_file(UcsalPlugin.root_path.join('config', 'communities.yml'))
    mapping[identifier] || [identifier]
  end

  def create_identifier(name, student=true)
    splited = name.split(" ")

    rules = ["first", "second", "third", "fourth"]

    identifier = nil

    rules.each do |rule|
      identifier = self.send(rule + "_rule", splited)
      break unless Person[identifier]
    end

    student ? identifier : identifier.gsub('-', '.')
  end

  def first_rule(splited)
    if splited.count > 1
      (splited.first + " " + splited.last).to_slug
    else
      splited.first.to_slug
    end
  end

  def second_rule(splited)
    if splited.count > 2
      (splited.first + " " + splited[1].first + " " + splited.last).to_slug
    else
      fourth_rule(splited)
    end
  end

  def third_rule(splited)
    if splited.count > 3
      (splited.first + " " + splited[1].first + " " + splited[-2].first + " " + splited.last).to_slug
    else
      fourth_rule(splited)
    end
  end

  def fourth_rule(splited)
    identifier = first_rule(splited)
    identifier = identifier + (Person.where("identifier ILIKE ?", "%#{identifier}%").count + 1).to_s
    identifier
  end

  def student?(login)
    login.blank? ? nil : login.first =~ /[[:digit:]]/
  end

  def body_ending
    '
    <!-- Codigo do Google para tag de remarketing -->
    <script type="text/javascript">
      var google_conversion_id = 936666708;
      var google_custom_params = window.google_tag_params;
      var google_remarketing_only = true;
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
      <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/936666708/?value=0&amp;guid=ON&amp;script=0"/>
      </div>
    </noscript>

    <!-- Google Code for Cadastro Conversion Page -->
    <script type="text/javascript">
      var google_conversion_id = 936666708;
      var google_conversion_language = "en";
      var google_conversion_format = "3";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "A54GCJfjpWEQ1MzRvgM";
      var google_remarketing_only = false;
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
      <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/936666708/?label=A54GCJfjpWEQ1MzRvgM&amp;guid=ON&amp;script=0"/>
      </div>
    </noscript>
    '
  end

  private

  def get_template(groups)
    profile = get_profile_type(groups)
    template_identifier = profiles_type[profile]
    template = Person[template_identifier]
  end

  def profiles_type
    {
      "Funcionarios" => 'modelo-funcionario',
      "Professores" => 'modelo-professor',
      "Estudantes" => 'modelo-estudante'
    }
  end
end
