# encoding: UTF-8

require_dependency 'account_controller'

class AccountController

  def change_password
    if request.post?
      #url = 'http://httpbin.org/post' # debug
      url = 'http://site1389987522.provisorio.ws/Mobile/troca_senha.php'
      Rails.logger.info "Requesting to change password... #{url}"
      success = false

      begin
        response = Net::HTTP.post_form URI(url), params["user"]
      rescue Exception => err
        response = err
      end
      if response.is_a? Net::HTTPSuccess
        Rails.logger.info 'Change password request OK.'
        success = true
        message = response.body.force_encoding("UTF-8")
      else
        Rails.logger.warn "Change password request FAIL. #{response}"
        success = false
        message = response
      end

      respond_to do |format|
        format.html { render :template => 'change_password_ucsal_resp', :locals => { :success => success, :message => message }}
      end
    else
      render file: 'change_password_ucsal'
    end
  end

  alias :forgot_password :change_password

end
