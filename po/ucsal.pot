# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc1-5-g286f8c3\n"
"POT-Creation-Date: 2015-10-22 18:23-0000\n"
"PO-Revision-Date: 2015-09-01 19:53-0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: plugins/ucsal/lib/ucsal_plugin.rb:7
msgid "Add Ucsal features."
msgstr ""

#: plugins/ucsal/views/change_password_ucsal_resp.html.erb:2
#: plugins/ucsal/views/change_password_ucsal.html.erb:1
msgid "Password Recovery"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal_resp.html.erb:12
msgid ""
"Sorry, there was an error in communication with the password change system."
msgstr ""

#: plugins/ucsal/views/change_password_ucsal_resp.html.erb:15
msgid "Please, contact the "
msgstr ""

#: plugins/ucsal/views/change_password_ucsal_resp.html.erb:17
msgid " to report on flaw and ask about other ways to change your password."
msgstr ""

#: plugins/ucsal/views/change_password_ucsal_resp.html.erb:22
msgid "Back"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal.html.erb:6
msgid "Enrollment:"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal.html.erb:7
msgid "Birth Date:"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal.html.erb:8
msgid "CPF:"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal.html.erb:10
msgid "Send solicitation"
msgstr ""

#: plugins/ucsal/views/change_password_ucsal.html.erb:15
msgid ""
"Your request will be forwarded to the Secretaria to generate a new password. "
"Please, wait for the email confirmation."
msgstr ""
