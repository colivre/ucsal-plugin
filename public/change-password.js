(function($){

  function inputNum() { this.value = this.value.replace(/[^0-9]/g, ''); }
  function validDate(el) {
    if ( /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/.test(el.value) ) {
      return true;
    } else {
      alert('Data inválida.');
      setTimeout(function(){ el.focus() }, 100);
      return false;
    }
  }

  $('#user_dt_nascimento').datepicker({
    altFormat: 'dd/mm/yy',
    defaultDate: '-20y',
    maxDate: '-10y'
  }).on('change', function() {
    if (!validDate(this)) return false;
  });

  $('#user_matricula').on('change', inputNum);

  $('#user_cpf').on('change', inputNum);

  $('#change-passwd-ucsal form').on('submit', function(){
    if (!validDate($('#user_dt_nascimento')[0])) return false;
    var inputs = $('#change-passwd-ucsal *[required]');
    for ( var input,n=0; input=inputs[n]; n++ ) {
      if (input.value == '') {
        input.focus();
        $(input).effect('highlight', 500);
        return false;
      }
    }
  });

})(jQuery);
